package test;

import com.selenium.java.base.BaseTest;
import org.testng.ITestContext;
import org.testng.annotations.*;

import static com.selenium.java.base.BaseTest.startAppiumServer;

public class DataProviderOLX extends BaseTest {
    @BeforeClass
    public void startup(){
        startAppiumServer();
    }
    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context){
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }
    @DataProvider
    public Object[][]getLogin() {
        return new Object[][]{
                {"tester_chester1@wp.pl", "tester"},
                {"tester_chester2@wp.pl", "testertester"},
                {"tester_chester3@wp.pl", "testerchester"},
                {"tester_chester@wp.pl", "miastochester"}
        };
    }
    @Test(dataProvider = "getLogin")
    public void lobby(String p1, String p2) {
        System.out.println("test");
        System.out.println(p1 + " " + p2 + " ");
    }
    @AfterMethod
    public void tearDown() {

    }
}
