package test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.AddScreen;
import com.selenium.java.pages.CreateAccountScreen;
import com.selenium.java.pages.LoginScreen;
import com.selenium.java.pages.SearchScreen;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class SimpleAndroidTest extends BaseTest {
    @BeforeClass
    public void startup(){

        startAppiumServer();
    }

    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String devicedId, String deviceName, ITestContext context){
        instalApp = false;

        launchAndroid(platform, devicedId, deviceName, context);
    }

    @Test
    public void test1(){
        Helper helper = new Helper();

        WebDriver driver = getDriver();
        //CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        LoginScreen registration = PageFactory.initElements(driver, LoginScreen.class);
        //SearchScreen searchScreen = PageFactory.initElements(driver,SearchScreen.class);
        //createAccountScreen.crateAccount();
        registration.loginToApp(" ", " ");
        //searchScreen.searching("volkswagen T3");
        //searchScreen.swipeDown();
        helper.getScreenShot("test Android.png");
    }
    @Test
    public void test2(){
        WebDriver driver = getDriver();
        LoginScreen registration = PageFactory.initElements(driver, LoginScreen.class);
        AddScreen addScreen = PageFactory.initElements(driver, AddScreen.class);
        registration.loginToApp(" ", " ");
        addScreen.add();
    }
    @Test
    public void test3(){
        WebDriver driver = getDriver();
        CreateAccountScreen createAccountScreen = PageFactory.initElements(driver, CreateAccountScreen.class);
        createAccountScreen.crateAccount();
    }
    @Test
    public void test4(){
        WebDriver driver = getDriver();
        LoginScreen registration = PageFactory.initElements(driver, LoginScreen.class);
        SearchScreen searchScreen = PageFactory.initElements(driver,SearchScreen.class);
        registration.loginToApp(" ", " ");
        searchScreen.searching("volkswagen T3");
        searchScreen.swipeDown();
        
    }



  //  @AfterMethod
  //  public void tearDown(){
  //      System.out.println("koniec...");
  //  }
}
