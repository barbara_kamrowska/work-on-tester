package test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.pages.LoginScreen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;

public class LoginAndroidTest extends BaseTest {
    @BeforeClass
    public void startup(){
        startAppiumServer();
    }
    @BeforeMethod
    @Parameters(value = {"Platform", "DeviceID", "DeviceName"})
    public void setUp(String platform, String deviceId, String deviceName, ITestContext context){
        instalApp = false;
        launchAndroid(platform, deviceId, deviceName, context);
    }
    @DataProvider
    public Object[][]getNoLogin() {
        return new Object[][]{
                {"tester_chester1@wp.pl", "tester"},
                {"tester_chester2@wp.pl", "testertester"},
                {"tester_chester3@wp.pl", "testerchester"},
                {"tester_chester3@wp.pl", "testerchester"}
        };
    }
    @DataProvider
    public Object[][]getLogin() {
        return new Object[][]{
                {"tester_chester@wp.pl", "miastochester"},
                {"tester_chester@wp.pl", "miastochester"},
                {"tester_chester@wp.pl", "miastochester"},
                {"tester_chester@wp.pl", "miastochester"}
        };
    }

    @Test(dataProvider = "getNoLogin")
    public void noLogin(String email, String password) {


        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
    }

    @Test(dataProvider = "getLogin")
    public void login(String email, String password) {

        System.out.println("test");
        System.out.println(email + " " + password + " ");

        WebDriver driver = getDriver();
        LoginScreen loginScreen = PageFactory.initElements(driver, LoginScreen.class);
        loginScreen.loginToApp(email, password);
    }

    @AfterMethod
    public void tearDown() {

    }
}
