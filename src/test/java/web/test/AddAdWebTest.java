package web.test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.helper.TestListener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.*;
import web.AddAnnouncementPage;
import web.HomePage;

import java.awt.*;


@Listeners ({TestListener.class})
public class AddAdWebTest extends BaseTest {

    Helper helper = new Helper();

    @BeforeMethod(description = "Dodawanie ogłoszenia - start")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false; //uncomment if u want a headless mode
        url = "https://www.olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"stół dędowy", "150", "stół dębowy", "Bocian Krzyś","/Users/Basia/Dkumenty/testPhotos/mieszkanie.jpg"},
                {"sofa mała", "50", "Roddam sofę", "Rita Flower", "/Users/Basia/Dkumenty/testPhotos/hortensja.jpg"},
                {"kieliszki", "5", "pozostałości po imprezie, cena za sztukę", "Ula Maliniak","/Users/Basia/Dkumenty/testPhotos/kieliszki-krysztalowe-czestochowa-516105089.jpg"},
                {"donica", "20", "Sprzedam donicę", "Mariola Kwiatkowska","/Users/Basia/Dkumenty/testPhotos/kot.jpg"},
                {"Drzwi drewniane", "50", "Stare drewniane drzwi", "Jan Fakt","/Users/Basia/Dkumenty/testPhotos/aktówka.jpg"}
        };
    }


    @Test(dataProvider = "getData",priority = 1, description = "Dodawanie ogłoszenia - testy")
    public void testAddAd(String tytul, String cena, String description, String user, String photolink) throws AWTException, InterruptedException {
        Helper helper = new Helper();

        WebDriver driver = getDriver();

        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        AddAnnouncementPage addAnnouncementPage = PageFactory.initElements(driver, AddAnnouncementPage.class);
        homePage.loginToAppWithData();
       // addAnnouncementPage.add();
        addAnnouncementPage.addWithData(tytul, cena, description, user, photolink);

        Assert.assertTrue(driver.findElement(By.id("save-from-preview")).isDisplayed());

        helper.getScreenShot("AddAd.png");
        helper.getScreenshotForAllure("AddAd.png");
    }

    @AfterMethod(description = "Testowanie dodawania ogłoszenia zakończone")
    public void tearDown() {

        WebDriver driver = getDriver();

        System.out.println("Test ended");
        //driver.close();
    }
}


