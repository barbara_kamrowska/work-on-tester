package web.test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.helper.TestListener;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import web.HomePage;
import web.SearchPage;

@Listeners({ TestListener.class })
public class SearchingWebTest extends BaseTest {

    Helper helper = new Helper();

    @BeforeMethod (description = "Wyszukiwanie - testy")
    @Parameters(value = {"Platform", "Browser"})
    @Step()
    public void setUp(String platform, String browser, ITestContext context) {

        headless = false;
        url = "https://olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }

    @DataProvider
    public Object[][] getData() {
        return new Object[][]{
                {"mieszkanie"},
                {"Transporter T3"},
                {"sukienka"},
                {"lodówka"},
                {"odkurzacz"},
                {"ekspres do kawy"}
        };
    }

    @Test(dataProvider = "getData", description = "Test prostego wyszukiwania")
    @Step()
    public void searching(String title) {

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        homePage.close();
        homePage.loginToAppWithData();
        searchPage.searching(title);
    }


    @Test(description = "Test wyszukiwania za pomocą filtrów")
    @Step()
    public void searchingFilters() throws InterruptedException {
        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        SearchPage searchPage = PageFactory.initElements(driver, SearchPage.class);
        homePage.close();
        searchPage.searchingFilters();
    }
    @AfterMethod (description = "Wyszukiwanie - testy zakończone")
    public void tearDown() {
        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();

    }
}

