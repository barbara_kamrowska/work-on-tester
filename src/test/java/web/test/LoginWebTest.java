package web.test;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import com.selenium.java.helper.TestListener;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.Story;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.*;
import web.HomePage;


@Listeners({TestListener.class})
public class LoginWebTest extends BaseTest {
    Helper helper = new Helper();

    @BeforeMethod (description = "Logowanie - testy")
    @Parameters(value = {"Platform", "Browser"})
    public void setUp(String platform, String browser, ITestContext context){

        headless = false;
        url = "https://olx.pl";

        launchWeb(platform, browser, context);
        System.out.println("Test started");
    }
    @DataProvider
    public Object[][]loginWithWrongData() {
        return new Object[][]{
                {"tester_chester1@wp.pl", "tester"},
 //               {"tester_chester2@wp.pl", "testertester"},
 //               {"tester_chester3@wp.pl", "testerchester"},
 //               {"tester_chester3@wp.pl", "testerchester"}
        };
    }

    @DataProvider
    public Object[][]loginWithNoData() {
        return new Object[][]{
                {"", ""},
  //              {"", ""},
 //               {"", ""},
 //               {"", ""}
        };
    }

    @DataProvider
    public Object[][]login() {
        return new Object[][]{
                {"tester_chester@wp.pl", "miastochester"},
 //               {"tester_chester@wp.pl", "miastochester"},
 //               {"tester_chester@wp.pl", "miastochester"},
 //               {"tester_chester@wp.pl", "miastochester"}
        };
    }

    @Test(dataProvider = "loginWithWrongData", description = "Test logowania z błędnymi danymi")
    @Severity(SeverityLevel.CRITICAL)
    @Step("Go to testLoginAccount")
    @Story("Correct username and correct password TEST")
    public void getLoginWithWrongData(String email, String password) {


        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.noLoginToApp(email, password);

        helper.getScreenShot("loginWithWrongData.png");
        helper.getScreenshotForAllure("loginWithWrongData");

    }

    @Test(dataProvider = "loginWithNoData", description = "Test logowania bez danych")
    @Severity(SeverityLevel.CRITICAL)
    @Step("Go to testLoginAccount")
    @Story("Correct username and correct password TEST")
    public void getLoginWithNoData(String email, String password) {


        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.loginToApp(email, password);

        helper.getScreenShot("loginWithNoData.png");
        helper.getScreenshotForAllure("loginWithNoData");
    }

    @Test(dataProvider = "login", description = "Test poprawnego logowania")
    @Severity(SeverityLevel.CRITICAL)
    @Step("Go to testLoginAccount")
    @Story("Correct username and correct password TEST")
    public void getLogin(String email, String password) {

        System.out.println("test");
        System.out.println(email + " " + password + " ");

        WebDriver driver = getDriver();
        HomePage homePage = PageFactory.initElements(driver, HomePage.class);
        homePage.loginToApp(email, password);

        helper.getScreenShot("login.png");
        helper.getScreenshotForAllure("login");
    }

    @AfterMethod (description = "Logowanie - testowanie zakończone")
    public void tearDown() {
        WebDriver driver = getDriver();

        System.out.println("Test ended");
        driver.close();
    }
}

