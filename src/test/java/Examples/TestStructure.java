package Examples;

import org.testng.annotations.*;

public class TestStructure {
    @BeforeClass
    public void startup() {
        System.out.println("I'm in BeforeClass");
    }

    @BeforeMethod
    public void setup() {
        System.out.println("I'm in BeforeMethod");
    }

    @Test()
    public void test1() {
        System.out.println("I'm in test1");
    }

    @Test()
    public void test2() {
        System.out.println("I'm in test2");
    }

    @Test()
    public void test3() {
        System.out.println("I'm in test3");
    }

    @Test()
    public void test4() {
        System.out.println("I'm in test4");
    }

    @AfterMethod
    public void tearDown() {
    }

    @AfterClass
    public void stop() {
    }

}
