package Examples;

public class Code_Example2 {
    public static void main(String[] args) {
        Person basia = new Person();

        basia.name = "Basia";
        basia.age = 27;
        basia.isAlive = true;

        Person mikolaj = new Person();
        mikolaj.name = "Mikołaj";
        mikolaj.age = 103;
        mikolaj.isAlive = false;

        basia.przedstawSie();
        mikolaj.przedstawSie();

        //////////////////////////////////

        Person marek = new Person();

        marek.przedstawSie( "Marek", 18, true);

        Person andrzej = new Person();

        andrzej.przedstawSie("Andrzej", 88, false);


    }


}
