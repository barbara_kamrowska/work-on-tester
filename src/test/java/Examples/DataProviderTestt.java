package Examples;

import org.testng.annotations.*;

public class DataProviderTestt {
    @BeforeClass
    public void startup() {
        System.out.println("I'm in BeforeClass");
    }
    @BeforeMethod
    public void setup() {
        System.out.println("I'm in BeforeMethod");
    }
    @DataProvider
    public Object[][]getData() {
        return new Object[][]{
                {5, "five", 1},
                {6, "six", 2},
                {7, "seven", 3},
                {8, "seven", 4}
        };
    }
    @Test(dataProvider = "getData")
    public void lobby(int p1, String p2, int p3) {

        System.out.println("test");
        System.out.println(p1 + " " + p2 + " " + p3);
    }

    @AfterMethod
    public void tearDown() {

    }
}
