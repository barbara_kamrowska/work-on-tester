package com.selenium.java.tests.presentation;

public class Compare_Strings_And_ifElse {

    public static void main(String[] args) {

compareString();


    //    compareStrings_Positive();
  //      compareStrings_Negative();
  //      compareStrings_Positive_2();
  //      compareStrings_Negative_2();

  //      compareStrings();

    }

    // region IF ELSE
    public static void compareStrings_Positive() {
        String object1 = "test";
        String object2 = "test";

        if (object1.equals(object2)) {
            System.out.println("Works and expected!");
        } else {
            System.out.println("Nope...");
        }
    }


    public static void compareStrings_Positive_2() {

            String object1 = "test";
            String object2 = "test";

            if (object1 == object2) { // nie zaleca się stosować == w Stringach
                System.out.println("Works as expected!");
            } else {
                System.out.println("Nope...");
            }
        }

    public static void compareStrings_Negative() {
        String object1 = "test";
        String object2 = "test_2";
        if (object1.equals(object2)) {
            System.out.println("Works as expected!");
        } else {
            System.out.println("Nope...");
        }
    }


    public static void compareString_Negative_2() {
                String object1 = "test";
                String object2 = "test";
                if (object1 != object2) { // nie zaleca się stosować != w Stringach
                    System.out.println("Works as expected!");

                } else {
                    System.out.println("Nope...");
                }
    }
    public static void compareString() {
        String object1 = "test";
        String object2 = "test2";
        String object3 = "test3";

        if (object1.equals(object2)) {
            System.out.println("object 1 = object 2");
        } else if (object1.equals(object3)) {
            System.out.println("object 1 = object 3");
        } else if (object2.equals(object3)) {
            System.out.println("object 2 = object 3");
        } else {
            System.out.println("Nope...Nope...");
        }
    }

}