package com.selenium.java.tests.presentation;

public class Conversions {

    public static void main(String[] args) {

        int a = 5;
        double b = 13.5;
        double c = b / a;

        System.out.println(c); // 2.7
        System.out.println(Math.round(c)); // 3


        //     int a = 5;
        //     double b = 13.5;
        //     int c = (int) b / a;

        //     System.out.println(c); // 2
    }
}
