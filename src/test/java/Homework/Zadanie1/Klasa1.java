//region Zadanie 1:
/*
Utwórz 2 klasy.
W pierwszej klasie wykonaj operacje, które pozwolą Tobie na obliczenie średniej ocen dla 3 uczniów:
Marek Waszak - oceny: 4, 3+, 5, 2+, 3+, 4, 5, 6, 3
Darek Pyrek - oceny: 2, 2+, 5, 6, 1, 3+, 4+, 2
Piotr Wąsik - oceny: 6, 6, 6, 4, 3+, 3, 1, 1, 1+, 5, 6

W drugiej klasie stwórz nowe obiekty dla poszczególnych uczniów i oblicz ich średnią.

UWAGA: Wynik proszę podać z max dwoma miejscami po przecinku.

Przykładowy outupt:
Średnia ocen dla ucznia Marek Waszak wynosi: 2.17
Średnia ocen dla ucznia Darek Pyrek wynosi: 2.75
Średnia ocen dla ucznia Piotr Wąsik wynosi: 3.56
*/
//endregion

package Homework.Zadanie1;

public class Klasa1 {
    public static void main(String[] args) {

        double[] oceny1 = {4, 3.5, 5, 2.5, 3.5, 4, 5, 6, 3};
        String name1 = "Marek Waszak";
        double[] oceny2 = {2, 2.5, 5, 6, 1, 3.5, 4.5, 2};
        String name2 = "Darek Pyrek";
        double[] oceny3 = {6, 6, 6, 4, 3.5, 3, 1, 1, 1.5, 5, 6};
        String name3 = "Piotr Młot";

        srednia(oceny1);
        srednia(oceny2);
        srednia(oceny3);

        System.out.format("Średnia ocen dla ucznia " + name1 + " wynosi " + "%.2f%n",srednia(oceny1));
        System.out.format("Średnia ocen dla ucznia " + name2 + " wynosi " + "%.2f%n",srednia(oceny2));
        System.out.format("Średnia ocen dla ucznia " + name3 + " wynosi " + "%.2f%n",srednia(oceny3));

    }

    public static double srednia(double[] oceny) {

        double sum = 0;

        for (int i = 0; i < oceny.length; i++) {
            sum = sum + oceny[i];
        }
        return sum/oceny.length;
    }





/*

    private enum Names {
        MAREK,
        DAREK,
        PIOTR
    }

    public static String name(Names names) {
        String name;

        switch (names) {
            case MAREK:
                name = "Marek Barek";
                break;
            case DAREK:
                name = "Darek Pyzio";
                break;
            case PIOTR:
                name = "Piotr Młot";
                break;

            default:
                name = "Unknown";

        }
        return name;
    }

    public static double oceny(Names names) {
        double oceny;
        switch (names) {
            case MAREK:
                double[] oceny1 = {4, 3.5, 5, 2.5, 3.5, 4, 5, 6, 3};

                return sum1;

                break;
            case DAREK:
                double[] oceny2 = {2, 2.5, 5, 6, 1, 3.5, 4.5, 2};

                return sum2;
                break;
            case PIOTR:

                double[] oceny3 = {6, 6, 6, 4, 3.5, 3, 1, 1, 1.5, 5, 6};
                return sum3;
                break;

            default:
                double sum1 = 0;
                double sum2 = 0;
                double sum3 = 0;



            for (double i = 0; i < oceny1.length; i++) {
                sum1 += oceny1[i];
            }

            for (double i = 0; i < oceny2.length; i++) {
                sum2 += oceny2[i];

            }
            for (double i = 0; i < oceny3.length; i++) {
                sum3 += oceny3[i];
            }



        }


    }

*/

}
