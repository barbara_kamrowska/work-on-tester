/*
Zadanie 2:
Stwórz tablice Stringów 10-elementową, w której umieścisz 10 wybranych za pomocą ENUMA imion.
Wyświetl wszystkie elementy z tablicy.
Wyświetl rozmiar tablicy.
Posegreguj alfabetycznie i wyświetl imiona w tablicy.

*/

package Homework.Zadanie1;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Klasa2 {

    public enum Names {
        ALA,
        OLA,
        MICHAL,
        WANDA,
        JAN,
        KAROL,
        BERNADETA,
        JAKUB,
        AGATA,
        PIOTR
    }
    public static void main(String[] args) {

        String[] tablesOfNames = new String[]{ChooseName(Names.ALA), ChooseName(Names.OLA), ChooseName(Names.MICHAL), ChooseName(Names.WANDA),
                ChooseName(Names.JAN), ChooseName(Names.KAROL), ChooseName(Names.BERNADETA), ChooseName(Names.JAKUB), ChooseName(Names.AGATA), ChooseName(Names.PIOTR)};

        System.out.println("Rozmiar tablicy to: " + tablesOfNames.length);
        System.out.println();

        System.out.println("Lista nieposortowana");
        for (int i = 0; i < tablesOfNames.length; i++) {
            System.out.println(tablesOfNames[i]);
        }
        System.out.println();
        System.out.println("Lista posortowana");
        Arrays.sort(tablesOfNames);

        for (int i = 0; i < tablesOfNames.length; i++) {
            System.out.println(tablesOfNames[i]);
        }
    }
    private static String ChooseName(Names names) {
        String name;

        switch (names) {

            case ALA:
                name = "Ania";
                break;
            case OLA:
                name = "Ola";
                break;
            case MICHAL:
                name = "Michał";
                break;
            case WANDA:
                name = "Wanda";
                break;
            case JAN:
                name = "Jaś";
                break;
            case KAROL:
                name = "Karol";
                break;
            case BERNADETA:
                name = "Benia";
                break;
            case JAKUB:
                name = "Kuba";
                break;
            case AGATA:
                name = "Agata";
                break;
            case PIOTR:
                name = "Piotr";
                break;
            default:
                name = "noname";
        }
        return name;
    }
}