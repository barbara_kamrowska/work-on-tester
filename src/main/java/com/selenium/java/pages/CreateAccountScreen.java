package com.selenium.java.pages;

import com.selenium.java.base.BaseTest;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CreateAccountScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_createAccount;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnBrowse")
    public WebElement btn_browse;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_registerMail;

    @FindBy(how = How.ID, using = "pl.tablica:id/textinput_placeholder")
    public WebElement input_eMail;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Załóż konto')]")
    public WebElement input_createAccount;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

   // @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Twoje hasło')]")
   // public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/closseButton")
    public WebElement btn_close;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_checkBox;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_login;

    public void crateAccount(){

        btn_createAccount.click();
        input_registerMail.sendKeys("test@wp.pl");
        //btn_password.click();
        input_password.sendKeys("test");
        btn_checkBox.click();
        btn_login.click();
    }
}
