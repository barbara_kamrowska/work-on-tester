package com.selenium.java.pages;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomeScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/createAccountBtn")
    public WebElement btn_createAccount;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnBrowse")
    public WebElement btn_browse;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_registerMail;

    @FindBy(how = How.ID, using = "pl.tablica:id/loginBtn")
    public WebElement btn_loginViaMail;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'E-mail')]")
    public WebElement input_email;

    @FindBy(how = How.ID, using = "pl.tablica:id/input")
    public WebElement input_password;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnLogInNew")
    public WebElement btn_login;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Załóż konto')]")
    public WebElement input_createAccount;

    @FindBy(how = How.ID, using = "pl.tablica:id/closseButton")
    public WebElement btn_close;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj')]")
    public WebElement btn_add;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj zdjęcia')]")
    public WebElement btn_addPicture;

    @FindBy(how=How.ID, using="pl.tablica:id/closeButton")
    public WebElement btn_logClose;


 /*   public void registrationToApp() {

        btn_createAccount.click();
        input_registerMail.sendKeys("tester_chester@wp.pl");
        input_password.sendKeys("miastochester");
        btn_login.click();


    }
    */

    public void loginToApp() {
        Helper helper = new Helper();

        btn_loginViaMail.click();

        input_email.sendKeys("tester_chester@wp.pl");
        input_password.sendKeys("miastochester");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        btn_login.click();
        btn_logClose.click();
        while (helper.swipeToElementByText("Dzisiaj")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
        }

    }



   /* public void swipeDown() {
        int i = 0;
        while (swipeToElementById("pl.tablica:id/chevronDown") && i < 2) {
            swipeInDirection(direction.UP, "up", 0.4);
            i++;
        } }

    */

}


