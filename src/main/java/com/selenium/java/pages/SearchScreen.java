package com.selenium.java.pages;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchScreen extends BaseTest {

    @FindBy(how = How.ID, using = "pl.tablica:id/searchInput")
    public WebElement btn_search;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'w Volkswagen')]")
    public WebElement input_search;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Filtruj')]")
    public WebElement input_filter;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Najtańsze')]")
    public WebElement input_choose;

    @FindBy(how = How.ID, using = "pl.tablica:id/checkbox")
    public WebElement btn_checkBox;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz')]")
    public WebElement input_chooser;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Cała Polska')]")
    public WebElement input_chooseLocation;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wybierz')]")
    public WebElement input_chooser1;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Transporter')]")
    public WebElement input_model;

    @FindBy(how = How.ID, using = "pl.tablica:id/btnSubmit")
    public WebElement btn_go;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Pokaż wyniki')]")
    public WebElement input_show;


    public void searching(String element) {
        Helper helper = new Helper();

        btn_search.sendKeys(element);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_search.click();
        input_search.click();
        input_filter.click();
        btn_checkBox.click();
        input_choose.click();
        input_chooser.click();
        input_chooseLocation.click();
        input_chooser1.click();
        while (helper.swipeToElementByText("Transporter")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
        }
        input_model.click();
        btn_go.click();
        input_show.click();
        }
        public void swipeDown() {
            Helper helper = new Helper();

            int i = 0;
            while (helper.swipeToElementByText("transporter") && i < 9) {
                helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
                i++;
            }
    }
}
