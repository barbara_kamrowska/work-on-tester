package com.selenium.java.pages;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class AddScreen extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj')]")
    public WebElement btn_add;

    @FindBy(how = How.ID, using = "pl.tablica:id/cancelButton")
    public WebElement btn_cancel;

    @FindBy(how = How.ID, using = "pl.tablica:id/phone_number")
    public WebElement btn_phoneNumber;

    @FindBy(how = How.ID, using = "pl.tablica:id/phone_number")
    public WebElement input_phoneNumber;

    @FindBy(how = How.ID, using = "pl.tablica:id/confirmButton")
    public WebElement btn_confirm;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Wpisz kod')]")
    public WebElement input_code;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Weryfikuj')]")
    public WebElement btn_ok;

    @FindBy(how = How.ID, using = "android:id/button1")
    public WebElement btn_gotowe;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Dodaj zdjęcia')]")
    public WebElement btn_addPicture;

    @FindBy(how = How.ID, using = "pl.tablica:id/img")
    public WebElement btn_picture;

    @FindBy(how = How.ID, using = "pl.tablica:id/action_done")
    public WebElement btn_done;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement btn_title;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_title;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_chooserCategory;

    @FindBy(how = How.ID, using = "pl.tablica:id/row")
    public WebElement btn_choose;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Prywatne')]")
    public WebElement btn_private;

    @FindBy(how = How.ID, using = "pl.tablica:id/chipGroup")
    public WebElement btn_prywatne;

    @FindBy(how = How.ID, using = "pl.tablica:id/priceTypeFree")
    public WebElement btn_priceFree;

    @FindBy(how = How.ID, using = "pl.tablica:id/priceInput")
    public WebElement input_price;

    @FindBy(how = How.XPATH, using = "//*[contains(@text, 'Nowe')]")
    public WebElement btn_new;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement btn_text;

    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_text;

    @FindBy(how = How.ID, using = "pl.tablica:id/chooserBtn")
    public WebElement btn_chooserLocation;

    @FindBy(how = How.ID, using = "pl.tablica:id/search_src_text")
    public WebElement input_location;

    @FindBy(how = How.ID, using = "pl.tablica:id/accept")
    public WebElement btn_accept;

    @FindBy(how = How.ID, using = "pl.tablica:id/previewAdBtn")
    public WebElement btn_preview;


    @FindBy(how = How.ID, using = "pl.tablica:id/inputText")
    public WebElement input_name;


    public void addNumber(){
        Helper helper = new Helper();

        btn_add.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_phoneNumber.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        input_phoneNumber.sendKeys("792801254");
        helper.pressAndroidBackBtn();
        btn_confirm.click();
        input_code.sendKeys("262796");
        btn_ok.click();
        btn_gotowe.click();
    }

    public void add(){
        Helper helper = new Helper();


        btn_add.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_addPicture.click();
        btn_picture.click();
        btn_done.click();
        btn_title.click();
        input_title.sendKeys("roślina");
        helper.pressAndroidBackBtn();
        btn_chooserCategory.click();
        btn_choose.click();
        while (helper.swipeToElementByText("Prywatnie lub firma*")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_private.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        btn_priceFree.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (helper.swipeToElementByText("Osoba kontaktowa")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
        }
        btn_new.click();
        btn_text.click();
        input_text.sendKeys("To jest roślina, której nie sprzedam.");
        while (helper.swipeToElementByText("Osoba kontaktowa")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.4);
        }
        btn_chooserLocation.click();
        input_location.sendKeys("Kraków");
        btn_accept.click();
        btn_accept.click();
        while (helper.swipeToElementByText("Zobacz przed dodaniem")) {
            helper.swipeInDirection(Helper.direction.UP, "up", 0.8);
        }
        input_name.sendKeys("Marcin");
        btn_preview.click();
    }
}
