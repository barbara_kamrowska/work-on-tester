package com.selenium.java.helper;

import com.selenium.java.base.BaseTest;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.time.Duration;

public class Helper extends BaseTest {

    public enum direction {
        DOWN,
        UP,
        LEFT,
        RIGHT
    }

    private int screenHeight;
    private int screenWidth;

    public int[] getResolutionHandler() {
        WebDriver driver = getDriver();
        Dimension size;
        size = driver.manage().window().getSize();
        screenHeight = (size.height);
        screenWidth = (size.width);
        return new int[]{screenHeight, screenWidth};
    }

    public void swipeInDirection(direction direction, String place, double power) {
        WebDriver driver = BaseTest.getDriver();
        int res[] = getResolutionHandler();
        int screenHeight = (int) Array.get(res, 0);
        int screenWidth = (int) Array.get(res, 1);
        int startY, startX, endX, endY;
        double multiplierX;
        double multiplierY;
        switch (place) {
            case "left":
                multiplierX = 0.2;
                multiplierY = 0.5;
                break;
            case "right":
                multiplierX = 0.8;
                multiplierY = 0.5;
                break;
            case "up":
                multiplierX = 0.5;
                multiplierY = 0.2;
                break;
            case "down":
                multiplierX = 0.5;
                multiplierY = 0.8;
                break;
            default:
                multiplierX = 0.5;
                multiplierY = 0.5;
                break;
        }
        switch (direction) {
            case DOWN:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * 0.2);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * power);
                break;
            case UP:
                startX = (int) (screenWidth * multiplierX);
                startY = (int) (screenHeight * power);
                endX = (int) (screenWidth * multiplierX);
                endY = (int) (screenHeight * 0.2);
                break;
            case RIGHT:
                startX = (int) (screenWidth * power);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * 0.2);
                endY = (int) (screenHeight * multiplierY);
                break;
            case LEFT:
                startX = (int) (screenWidth * 0.2);
                startY = (int) (screenHeight * multiplierY);
                endX = (int) (screenWidth * power);
                endY = (int) (screenHeight * multiplierY);
                break;
            default:
                throw new IllegalArgumentException("Incorrect direction: " + direction);
        }
        new TouchAction((PerformsTouchActions) driver).press(PointOption.point(startX, startY))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(500)))
                .moveTo(PointOption.point(endX, endY)).release().perform();
    }

    public boolean swipeToElementById(String id) {
        WebDriverWait wait = getWaitDriver(1);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public boolean swipeToElementByText(String text) {
        WebDriverWait wait = getWaitDriver(2);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[contains(@text, '" + text + "')]")));
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    public void getScreenShot(String text) {
        WebDriver driver = getDriver();
        File file = new File("imagesFromTests/");
        String imagePath = file.getAbsolutePath();
        File srcFile;
        File targetFile;
        String pathToNewFile = imagePath + "/" + text;
        srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        targetFile = new File(pathToNewFile);
        try {
            FileUtils.copyFile(srcFile, targetFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void getScreenshotForAllure(String text) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();
        if (driver instanceof WebDriver) {
            testListener.saveScreenshotPNG(driver);
        }
    }

   /* public void testScreenShoot(String classname) {
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();
        if (driver instanceof  WebDriver) {
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Test: "+ classname + " failed and screenshot taken!");
    }

    */

   /* public void testScreenShot(String testname){
        WebDriver driver = getDriver();
        TestListener testListener = new TestListener();
        if (driver instanceof WebDriver){
            testListener.saveScreenshotPNG(driver);
        }
        testListener.saveTextLog("Test "+testname+" end and screenshot taken!");
    }

    */

    public void pressAndroidBackBtn() {
        WebDriver driver = BaseTest.getDriver();
        ((AndroidDriver) driver).pressKey(new KeyEvent(AndroidKey.BACK));
    }

    public void waitTime(int czas) {
        try {
            Thread.sleep(czas * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public void clickCoordinates(double point_x, double point_y) {
        WebDriver driver = getDriver();
        Actions action = new Actions(driver);
        Actions moveMauseToClick = action
                .moveByOffset((int) point_x, (int) point_y).click();
        moveMauseToClick.perform();
    }
}
