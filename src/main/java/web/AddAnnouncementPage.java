package web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

public class AddAnnouncementPage extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[@id=\"postNewAdLink\"]/span")
    public WebElement btn_add;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-title\"]")
    public WebElement btn_title;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-title\"]")
    public WebElement input_title;

    @FindBy(how = How.XPATH, using = "//*[@id=\"targetrenderSelect1-0\"]/dt")
    public WebElement btn_category;

    @FindBy(how = How.XPATH, using = "//*[@id=\"cat-103\"]/span[1]")
    public WebElement btn_animals;

    @FindBy(how = How.XPATH, using = "//*[@id=\"category-103\"]/div[2]/div[2]/div/ul/li[2]/a")
    public WebElement btn_cats;

    @FindBy(how = How.XPATH, using = "//*[@id=\"category-138\"]/div[2]/div[2]/div/ul/li[2]/a")
    public WebElement btn_choose;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-file-1\"]/div/a")
    public WebElement btn_addPhoto;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-description\"]")
    public WebElement btn_description;

    @FindBy(how = How.XPATH, using = "//*[@id=\"add-description\"]")
    public WebElement input_description;

    @FindBy(how = How.ID, using = "cat-584")
    public WebElement btn_domOgrod;

   // @FindBy(how = How.XPATH, using = "category-628//*[@id="category-628"]/div[2]/div[2]/div/ul/li[9]/a")
   // public WebElement btn_pozostale;

    @FindBy(how = How.ID, using = "parameter-div-price")
    public WebElement btn_price;

    @FindBy(how = How.ID, using = "targetid_private_business")
    public WebElement btn_osobaPrywatna;

    @FindBy(how = How.ID, using = "mapAddress")
    public WebElement input_localization;

    @FindBy(how = How.ID, using = "add-person")
    public WebElement input_name;

    @FindBy(how = How.ID, using = "add-phone")
    public WebElement input_phone;

    @FindBy(how = How.ID, using = "preview-link")
    public WebElement btn_checkAd;


    //*[@id="cat-1307"]/span[1]

    //*[@id="cat-584"]/span[1]


    public void add() throws InterruptedException, AWTException {
        btn_add.click();
        btn_title.click();
        input_title.sendKeys("gruby kot");
        btn_category.click();
        btn_animals.click();
        btn_cats.click();
        btn_choose.click();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // btn_addPhoto.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Thread.sleep(2000);
        File file = new File("/Users/Basia/Dkumenty/testPhotos/kot.jpg");
        StringSelection stringSelection = new StringSelection(file.getAbsolutePath());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.keyPress(KeyEvent.VK_G);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.keyRelease(KeyEvent.VK_G);
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        btn_description.click();
        input_description.sendKeys("Oddam puszystego kota.");
    }

    public void addWithData(String tytul, String cena, String description, String user, String photoLink) throws AWTException, InterruptedException {
        Helper helper = new Helper();


        btn_add.click();
        btn_title.click();
        input_title.sendKeys(tytul);
        btn_category.click();
       // helper.clickCoordinates(300, 500);
        btn_domOgrod.click();
        //btn_pozostale.click();
        btn_price.sendKeys(cena);
        btn_osobaPrywatna.click();
        btn_description.click();
        input_description.sendKeys(description);
        btn_addPhoto.click();
        wait();

        Thread.sleep(2000);
        File file = new File(photoLink);
        StringSelection stringSelection = new StringSelection(file.getAbsolutePath());
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_TAB);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_TAB);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_SHIFT);
        robot.keyPress(KeyEvent.VK_G);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_SHIFT);
        robot.keyRelease(KeyEvent.VK_G);
        robot.keyPress(KeyEvent.VK_META);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_META);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.delay(500);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);

        input_localization.sendKeys("86-031");
        input_name.sendKeys(user);
        input_phone.sendKeys("453920878");
        btn_checkAd.click();


    }
}
