package web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class HomePage extends BaseTest {

    @FindBy(how = How.XPATH, using = "/html/body")
    public WebElement btn_close;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cookiesBar\"]/button")
    public WebElement btn_closeCookie;
    @FindBy(how = How.XPATH, using = "//*[@id=\"topLoginLink\"]/i")
    public WebElement btn_account;
    @FindBy(how = How.XPATH, using = "//*[@id=\"userEmail\"]")
    public WebElement input_email;
    @FindBy(how = How.XPATH, using = "//*[@id=\"userPass\"]")
    public WebElement input_password;
   // @FindBy(how = How.XPATH, using = "//*[@id=\"se_userLogin\"]")
   // public WebElement btn_login;
    @FindBy(how = How.ID, using = "se_userLogin")
    public WebElement btn_login;

    @FindBy(how = How.XPATH, using = "//*[@id=\"headerLogo\"]")
    public WebElement btn_olx;
    @FindBy(how = How.XPATH, using = "//*[@id=\"headerSearch\"]")
    public WebElement btn_search;
    @FindBy(how = How.XPATH, using = "//*[@id=\"headerSearch\"]")
    public WebElement input_search;



    public void close(){
        btn_close.click();
        btn_closeCookie.click();
    }
    public void loginToApp(String email, String password) {
        Helper helper = new Helper();

        helper.waitTime(2);

        btn_account.click();
        input_email.sendKeys(email);
        input_password.sendKeys(password);
        helper.waitTime(2);

        btn_closeCookie.click();
        btn_login.click();
    }

    public void noLoginToApp(String email, String password) {
        Helper helper = new Helper();

        helper.waitTime(2);

        btn_account.click();
        input_email.sendKeys(email);
        input_password.sendKeys(password);
        helper.waitTime(2);

        btn_closeCookie.click();
        btn_login.click();
    }
    public void loginToAppWithData(){

        btn_account.click();
        input_email.sendKeys("tester_chester@wp.pl");
        input_password.sendKeys("miastochester");
      //  btn_closeCookie.click();
        btn_login.click();

    }
}
