package web;

import com.selenium.java.base.BaseTest;
import com.selenium.java.helper.Helper;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class SearchPage extends BaseTest {

    @FindBy(how = How.XPATH, using = "//*[@id=\"headerLogo\"]")
    public WebElement btn_olx;

    @FindBy(how = How.XPATH, using = "//*[@id=\"searchmain-container\"]/div[1]/div/div[1]/div[2]/div/a/span[2]")
    public WebElement btn_nieruchomosci;

    @FindBy(how = How.XPATH, using = "//*[@id=\"bottom3\"]/ul/li[1]/a/span/span")
    public WebElement btn_mieszkania;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_subcat\"]/div[2]/ul/li[3]/a")
    public WebElement btn_forSale;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_builttype\"]/div[2]/a/span[1]")
    public WebElement btn_builtType;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/a/span[1]")
    public WebElement input_price1;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[1]/a/span[1]")
    public WebElement btn_price1;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]")
    public WebElement input_price2;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_price\"]/div[2]/div[2]")
    public WebElement btn_price2;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_subcat\"]/div[2]/a/span[2]")
    public WebElement btn_choose1;

    @FindBy(how = How.XPATH, using = "//*[@id=\"param_builttype\"]/div[2]/a/span[2]")
    public WebElement btn_choose2;

    @FindBy(how = How.XPATH, using = "//*[@id=\"headerSearch\"]")
    public WebElement btn_search;

    @FindBy(how = How.XPATH, using = "//*[@id=\"headerSearch\"]")
    public WebElement input_search;



    public void searching(String title) {
        Helper helper = new Helper();

        helper.waitTime(2);
        btn_olx.click();
        btn_search.click();
        input_search.sendKeys(title);
    }

    public void searchingFilters () throws InterruptedException {
        Helper helper = new Helper();

        helper.waitTime(2);

        btn_olx.click();
        btn_nieruchomosci.click();
        btn_mieszkania.click();
        btn_choose1.click();
        btn_forSale.click();
        btn_choose2.click();
        btn_builtType.click();

       // helper.wait(2);
       // btn_price1.click();
       // input_price1.sendKeys("50000");
       // helper.wait(2);
       // btn_price2.click();
       // input_price2.sendKeys("200000");
       // helper.wait(2);

        //input_price1.sendKeys("50000");
        //input_price2.sendKeys("200000");
    }



}
